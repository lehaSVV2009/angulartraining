peopleApp.controller('ContentCtrl', ['$scope', function (scope) {

    // show Content Button
    scope.showContent = true;

    scope.$watch('showContent', function () {
    	scope.toggleButtonText = scope.showContent ? 'HIDE_LESSON_CONTENT_BUTTON' : 'SHOW_LESSON_CONTENT_BUTTON';
    });

    
}]);