var peopleApp = angular.module('peopleApp', ['pascalprecht.translate']);

peopleApp.config(function ($translateProvider) {

  	$translateProvider.translations('en', {
    	
		LESSON_ONE_TITLE: 'Lesson 1. Homework',

		HIDE_LESSON_CONTENT_BUTTON: 'Hide Lesson Content',
		SHOW_LESSON_CONTENT_BUTTON: 'Show Lesson Content',

		FILTER_PEOPLE_BUTTON: 'Enter text to filter people',

		FIRST_NAME_COLUMN: 'First Name',
		LAST_NAME_COLUMN: 'Last Name',
		AGE_COLUMN: 'Age',
		CITY_COLUMN: 'City',
		HOME_PHONE_NUMBER_COLUMN: 'Home Phone Number',
		FAX_NUMBER_COLUMN: 'Fax Number',
		ACTIONS_COLUMN: 'Actions',

		ENGLISH: 'English',
		RUSSIAN: 'Russian'

  	});

  	$translateProvider.translations('ru', {
    	
		LESSON_ONE_TITLE: 'Занятие 1. ДЗ',

		HIDE_LESSON_CONTENT_BUTTON: 'Спрятать содержание',
		SHOW_LESSON_CONTENT_BUTTON: 'Показать содержание',

		FILTER_PEOPLE_BUTTON: 'Фильтр',

		FIRST_NAME_COLUMN: 'Имя',
		LAST_NAME_COLUMN: 'Фамилия',
		AGE_COLUMN: 'Возраст',
		CITY_COLUMN: 'Город',
		HOME_PHONE_NUMBER_COLUMN: 'Домашний номер',
		FAX_NUMBER_COLUMN: 'Номер факса',
		ACTIONS_COLUMN: 'Действия',

		ENGLISH: 'Английский',
		RUSSIAN: 'Русский'
  	});


  	$translateProvider.preferredLanguage('ru');
});